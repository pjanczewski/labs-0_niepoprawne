﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Wilk : Zwierze
    {
        Boolean noc;

        public Wilk()
        {
            this.Nazwa = "Wilk";
            this.Wycie = "Auuuuuuuuuuuuuu!";
            this.noc = false;
        }

        // zlamanie zasady z wikipedi:
        // Warunki nie mogą byc wzmocnione w podtypach
        // poniewaz wycie zwierzaka zalezy od nocy
        // wiec dodalismy pewne zaleznosci w ktorych ta metoda bedzie dzialac lub nie
        // przez dodanie tego warunku zmienilismy glowna funkcjonalnosc klasy Zwierze
        public override string Zawyj()
        {
            if (this.noc)
                return base.Zawyj();
            else
                return "";
        }
    }
}
