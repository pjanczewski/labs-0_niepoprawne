﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Zwierze
    {
        public Zwierze() { }

        protected string Wycie = "Wycie zwierza";
        protected string Nazwa = "";

        // metoda zwracajaca wycie zwierzecia
        public virtual string Zawyj()
        {
            return this.Wycie;
        }

        public override string ToString()
        {
            return this.Nazwa + " " + this.Zawyj();
        }


    }
}
